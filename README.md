# Triton Inference Server Demo

This is a hands-on demo for the Triton Inference Server.


# Architecture
TODO


# Setup

## Requirements

### Hardware
Triton Inference Server is great when used with GPU resources.  
If no GPU is available: [link](https://docs.nvidia.com/deeplearning/sdk/triton-inference-server-guide/docs/run.html#running-the-inference-server-on-a-system-without-a-gpu)

### Tools & librairies
To run docker containers:
  - [Docker](https://docs.docker.com/engine/install/)
  - [Nvidia-docker](https://github.com/NVIDIA/nvidia-docker) (for GPU support)
  - [Cuda](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html) (for GPU support)

To run the client
  - python 3.7
  - [pipenv](https://github.com/pypa/pipenv)

### Nvidia NGC account
You will need an Nvidia NGC account and an API key to download the prebuilt docker images.
Create an account and generate an API key [here](https://www.nvidia.com/fr-fr/gpu-cloud/).

### Object detection model
A pre-trained model is used by the inference server. [Download the model](http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v2_quantized_300x300_coco_2019_01_03.tar.gz) from the Tensorflow pre-trained models.
ssd_mobilenet_v2_quantized_coco

- Clone the repository and build the client
```commandline
git clone https://github.com/NVIDIA/triton-inference-server.git
cd triton-inference-server
docker build -t tritonserver_client -f Dockerfile.client . 
```

- Python environment for client
```commandline
pipenv setup
```


# Running the server

Log in to the Nvidia container registry
```commandline
$ docker login nvcr.io
Username: $oauthtoken
Password: <Your Key>
```

Start the inference server
```commandline
$ docker run --rm --gpus all --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 -p8000:8000 -p8001:8001 -p8002:8002 -v models:/models  nvcr.io/nvidia/tritonserver:20.03-py3 trtserver --model-repository=/models --strict-model-config=false
```

The nvidia-docker -v option maps the local model directory into the container at /models, and the --model-repository option to the server is used to point to /models as the model repository.

The -p flags expose the container ports where the inference server listens for HTTP requests (port 8000), listens for GRPC requests (port 8001), and reports Prometheus metrics (port 8002).

The --shm-size and --ulimit flags are recommended to improve the server’s performance. For --shm-size the minimum recommended size is 1g but smaller or larger sizes may be used depending on the number and size of models being served.

The --strict-model-config flags allows to avoid adding a config.pbtxt file with the model to serve. See [model configuration](https://docs.nvidia.com/deeplearning/sdk/triton-inference-server-master-branch-guide/docs/model_configuration.html#section-generated-model-configuration).

# Client size
TODO with webcam

```commandline
curl localhost:8000/api/health/live
```

# Documentation
- [Nvidia documentation](https://docs.nvidia.com/deeplearning/sdk/triton-inference-server-guide)
- [Github repository](https://github.com/NVIDIA/triton-inference-server)
- [Tensorflow object detection pretrained models](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md)
