import argparse
import sys

import requests


class InferenceServerClient:
    def __init__(self):
        self.base_url = 'http://localhost:8000'

        parser = argparse.ArgumentParser(
            description='Client for Triton Inference Server')
        parser.add_argument('command', help='Subcommand to run')
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)

        getattr(self, args.command)()

    def get_health(self):
        api_url = f'{self.base_url}/api/health/ready'
        response = requests.get(url=api_url)
        self._parse_response(response)

    def get_status(self):
        api_url = f'{self.base_url}/api/status'
        response = requests.get(url=api_url)
        self._parse_response(response)

    def predict_image(self):
        parser = argparse.ArgumentParser(
            description='Predict image on model')
        parser.add_argument('filepath', help='Filepath to image to predict')
        parser.add_argument('model_name', help='Model name to run the inference')
        args = parser.parse_args(sys.argv[2:])

        api_url = f'{self.base_url}/api/infer/{args.model_name}'

        headers = 0 # A definir avec https://github.com/NVIDIA/triton-inference-server/blob/master/src/clients/python/api_v1/examples/image_client.py

    def _parse_response(self, response):
        print(f'Status code: {response.status_code}')
        print(f'Content: \n{response.text}')


if __name__ == '__main__':
    InferenceServerClient()
